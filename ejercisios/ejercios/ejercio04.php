<!DOCTYPE html>
<html>
  <head>
    <title>Cantidad de números primos de n cifras</title>
    <script>
      function contarPrimos(n) {
        let count = 0;
        const lowerBound = Math.pow(10, n - 1);
        const upperBound = Math.pow(10, n) - 1;

        for (let i = lowerBound; i <= upperBound; i++) {
          let isPrime = true;
          for (let j = 2; j < i; j++) {
            if (i % j === 0) {
              isPrime = false;
              break;
            }
          }
          if (isPrime) {
            count++;
          }
        }

        return count;
      }

      function mostrarResultado() {
        const n = document.getElementById("n").value;
        const cantidadPrimos = contarPrimos(n);
        document.getElementById("resultado").innerHTML =
          "La cantidad de números primos de " +
          n +
          " cifras es: " +
          cantidadPrimos;
      }
    </script>
  </head>
  <body>
    <h1>Cantidad de números primos de n cifras</h1>
    <label for="n">Ingrese la cantidad de cifras:</label>
    <input type="number" id="n" name="n" min="1" max="20" value="3" />
    <button onclick="mostrarResultado()">Contar</button>
    <p id="resultado"></p>
  </body>
</html>