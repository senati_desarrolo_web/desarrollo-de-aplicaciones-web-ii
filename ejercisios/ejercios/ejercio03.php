
<!DOCTYPE html>
<html>
  <head>
    <title>Números capicúa</title>
  </head>
  <body>
    <h1>Números capicúa</h1>
    <p>Introduce el rango de números:</p>
    <form>
      <label for="inicio">Inicio:</label>
      <input type="number" id="inicio" name="inicio">
      <br>
      <label for="fin">Fin:</label>
      <input type="number" id="fin" name="fin">
      <br>
      <button type="button" onclick="contarCapicuas()">Contar capicúas</button>
    </form>
    <p id="resultado"></p>
    <script>
      function esCapicua(numero) {
        // Convierte el número a texto y luego lo compara con su reverso
        return numero.toString() === numero.toString().split("").reverse().join("");
      }
      
      function contarCapicuas() {
        // Obtiene los valores de inicio y fin
        var inicio = parseInt(document.getElementById("inicio").value);
        var fin = parseInt(document.getElementById("fin").value);
        
        // Verifica que los valores sean válidos
        if (isNaN(inicio) || isNaN(fin)) {
          document.getElementById("resultado").innerHTML = "Introduce un rango válido";
          return;
        }
        
        // Inicializa el contador
        var contador = 0;
        
        // Recorre los números dentro del rango y verifica si son capicúas
        for (var i = inicio; i <= fin; i++) {
          if (esCapicua(i)) {
            contador++;
          }
        }
        
        // Muestra el resultado
        document.getElementById("resultado").innerHTML = "Hay " + contador + " números capicúa en el rango " + inicio + " - " + fin;
      }
    </script>
  </body>
</html>