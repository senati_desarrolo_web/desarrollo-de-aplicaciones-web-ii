// Dado un rango numérico entero positivo a y b, obtener la suma y la cantidad de los números
// pares, impares y múltiplos de 3.
<?php
//Variables
$s_inicia_a = 0;
$s_finaliza_b = 0;
$suma= 0;

if (isset($_POST["btnCalcular"])) {
    $s_inicia_a = (int)$_POST["txtn1"];
    $s_finaliza_b = (int)$_POST["txtn2"];
    for($i = $s_inicia_a ; $i <= $s_finaliza_b; $i++){
        $suma = $suma + $i;
    }
}
?>

<html>

<head>
    <title>Suma de cuadrados y cubos </title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>
    <form method="post" action="ejercio02.php">
        <table width="241" border="0">
</body>
<tr>
    <td colspan="2"><strong>Suma de cuadrados y cubos</strong> </td>
</tr>
<tr>
    <td width="81">Ingrese inicial: </td>
    <td width="150">
        <input name="txtn1" type="text" id="txtn1" value="<?= $s_inicia_a ?>" />
    </td>
</tr>

<tr>
    <td width="81">Ingrese final: </td>
    <td width="150">
        <input name="txtn2" type="text" id="txtn2" value="<?= $s_finaliza_b ?>" />
    </td>
</tr>

<tr>
    <td>Suma:</td>
    <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $suma ?>" />
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td>
        <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
    </td>
</tr>
</table>
</form>
</body>

</html>
