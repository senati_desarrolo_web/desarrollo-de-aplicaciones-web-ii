<!DOCTYPE html>
<html>
<head>
	<title>Contar palabras palíndromas</title>
	<script>
		function contarPalindromas() {
			// Obtener la frase del usuario
			var frase = document.getElementById("frase").value;

			// Convertir la frase en un array de palabras
			var palabras = frase.split(" ");

			// Inicializar el contador de palabras palíndromas
			var contador = 0;

			// Iterar sobre cada palabra en el array
			for (var i = 0; i < palabras.length; i++) {
				// Obtener la palabra actual y su reverso
				var palabraActual = palabras[i];
				var palabraReverso = palabraActual.split("").reverse().join("");

				// Comparar la palabra actual con su reverso para determinar si es un palíndromo
				if (palabraActual === palabraReverso) {
					contador++;
				}
			}

			// Mostrar el resultado en la página
			document.getElementById("resultado").innerHTML = "Se encontraron " + contador + " palabras palíndromas.";
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="frase">Ingrese una frase:</label>
	<input type="text" id="frase"><br>

	<button onclick="contarPalindromas()">Contar palabras palíndromas</button>

	<div id="resultado"></div>
</body>
</html>
