<!DOCTYPE html>
<html>
<head>
	<title>Buscar palabra en frase</title>
	<script>
		function buscarPalabra() {
			// Obtener la frase y la palabra del usuario
			var frase = document.getElementById("frase").value;
			var palabra = document.getElementById("palabra").value;

			// Buscar la palabra en la frase
			var resultado = frase.includes(palabra);

			// Mostrar el resultado en la página
			if (resultado) {
				document.getElementById("resultado").innerHTML = "La palabra \"" + palabra + "\" se encontró en la frase.";
			} else {
				document.getElementById("resultado").innerHTML = "La palabra \"" + palabra + "\" no se encontró en la frase.";
			}
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="frase">Ingrese una frase:</label>
	<input type="text" id="frase"><br>

	<label for="palabra">Ingrese una palabra:</label>
	<input type="text" id="palabra"><br>

	<button onclick="buscarPalabra()">Buscar palabra</button>

	<div id="resultado"></div>
</body>
</html>
