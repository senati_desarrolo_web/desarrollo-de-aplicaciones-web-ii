<!DOCTYPE html>
<html>
<head>
	<title>Números múltiplos de n</title>
	<script>
		function contarMultiplos() {
			
			var num1 = parseInt(document.getElementById("num1").value);
			var num2 = parseInt(document.getElementById("num2").value);
			var num3 = parseInt(document.getElementById("num3").value);
			var num4 = parseInt(document.getElementById("num4").value);
			var num5 = parseInt(document.getElementById("num5").value);
			var num6 = parseInt(document.getElementById("num6").value);
			var n = parseInt(document.getElementById("n").value);

			
			var numeros = [num1, num2, num3, num4, num5, num6];

			
			var contador = 0;
			for (var i = 0; i < numeros.length; i++) {
				if (numeros[i] % n == 0) {
					contador++;
				}
			}

			
			document.getElementById("resultado").innerHTML = "Ha ingresado " + contador + " números múltiplos de " + n;
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="num1">Número 1:</label>
	<input type="number" id="num1"><br>

	<label for="num2">Número 2:</label>
	<input type="number" id="num2"><br>

	<label for="num3">Número 3:</label>
	<input type="number" id="num3"><br>

	<label for="num4">Número 4:</label>
	<input type="number" id="num4"><br>

	<label for="num5">Número 5:</label>
	<input type="number" id="num5"><br>

	<label for="num6">Número 6:</label>
	<input type="number" id="num6"><br>

	<label for="n">Número para comprobar si son múltiplos:</label>
	<input type="number" id="n"><br>

	<button onclick="contarMultiplos()">Contar</button>

	<div id="resultado"></div>
</body>
</html>
