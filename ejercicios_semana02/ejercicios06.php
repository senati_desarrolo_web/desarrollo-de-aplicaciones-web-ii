<!DOCTYPE html>
<html>
<head>
	<title>Mensaje de bienvenida</title>
	<script>
		function mostrarMensaje() {
			// Obtener el nombre de la persona
			var nombre = document.getElementById("nombre").value;

			// Mostrar el mensaje de bienvenida en la página
			document.getElementById("mensaje").innerHTML = "Bienvenido, Sr(a) " + nombre + ", a su tienda de preferencia.";
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="nombre">Ingrese su nombre:</label>
	<input type="text" id="nombre">

	<button onclick="mostrarMensaje()">Mostrar mensaje</button>

	<div id="mensaje"></div>
</body>
</html>
