<!DOCTYPE html>
<html>
<head>
	<title>Multiplicación de matrices</title>
	<script>
		function multiplicarMatrices() {
			
			var A = [
				[1, 2],
				[3, 4]
			];
            var B = [
				[5, 6],
				[7, 8]
			];
            var C = [
				[0, 0],
				[0, 0]
			];
            for (var i = 0; i < 2; i++) {
				for (var j = 0; j < 2; j++) {
					for (var k = 0; k < 2; k++) {
						C[i][j] += A[i][k] * B[k][j];
					}
				}
			}

			
			document.getElementById("resultado").innerHTML = "La matriz C es [[" + C[0][0] + ", " + C[0][1] + "], [" + C[1][0] + ", " + C[1][1] + "]]";
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<button onclick="multiplicarMatrices()">Multiplicar matrices</button>

	<div id="resultado"></div>
</body>
</html>
