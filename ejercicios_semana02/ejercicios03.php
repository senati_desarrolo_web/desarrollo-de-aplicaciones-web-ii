<!DOCTYPE html>
<html>
<head>
	<title>Números repetidos</title>
	<script>
		function contarRepetidos() {
			// Obtener los valores ingresados por el usuario
			var num1 = parseInt(document.getElementById("num1").value);
			var num2 = parseInt(document.getElementById("num2").value);
			var num3 = parseInt(document.getElementById("num3").value);
			var num4 = parseInt(document.getElementById("num4").value);
			var num5 = parseInt(document.getElementById("num5").value);
			var num6 = parseInt(document.getElementById("num6").value);

			// Almacenar los valores en un arreglo
			var numeros = [num1, num2, num3, num4, num5, num6];

			// Contar cuántos números son repetidos
			var repetidos = {};
			for (var i = 0; i < numeros.length; i++) {
				if (numeros[i] in repetidos) {
					repetidos[numeros[i]]++;
				} else {
					repetidos[numeros[i]] = 1;
				}
			}

			var contador = 0;
			for (var numero in repetidos) {
				if (repetidos[numero] > 1) {
					contador++;
				}
			}

			// Mostrar el resultado en la página
			document.getElementById("resultado").innerHTML = "Ha ingresado " + contador + " números repetidos";
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="num1">Número 1:</label>
	<input type="number" id="num1"><br>

	<label for="num2">Número 2:</label>
	<input type="number" id="num2"><br>

	<label for="num3">Número 3:</label>
	<input type="number" id="num3"><br>

	<label for="num4">Número 4:</label>
	<input type="number" id="num4"><br>

	<label for="num5">Número 5:</label>
	<input type="number" id="num5"><br>

	<label for="num6">Número 6:</label>
	<input type="number" id="num6"><br>

	<button onclick="contarRepetidos()">Contar</button>

	<div id="resultado"></div>
</body>
</html>

