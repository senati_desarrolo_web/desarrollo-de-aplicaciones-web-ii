<!DOCTYPE html>
<html>
<head>
	<title>Promedio de una matriz</title>
	<script>
		function obtenerPromedio() {
			
			var num1 = parseInt(document.getElementById("num1").value);
			var num2 = parseInt(document.getElementById("num2").value);
			var num3 = parseInt(document.getElementById("num3").value);
			var num4 = parseInt(document.getElementById("num4").value);
			var num5 = parseInt(document.getElementById("num5").value);
			var num6 = parseInt(document.getElementById("num6").value);

			
			var matriz = [
				[num1, num2],
				[num3, num4],
				[num5, num6]
			];

			
			var suma = 0;
			for (var i = 0; i < matriz.length; i++) {
				for (var j = 0; j < matriz[i].length; j++) {
					suma += matriz[i][j];
				}
			}
			var promedio = suma / (matriz.length * matriz[0].length);

			
			document.getElementById("resultado").innerHTML = "El promedio de la matriz es " + promedio;
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="num1">Número 1:</label>
	<input type="number" id="num1"><br>

	<label for="num2">Número 2:</label>
	<input type="number" id="num2"><br>

	<label for="num3">Número 3:</label>
	<input type="number" id="num3"><br>

	<label for="num4">Número 4:</label>
	<input type="number" id="num4"><br>

	<label for="num5">Número 5:</label>
	<input type="number" id="num5"><br>

	<label for="num6">Número 6:</label>
	<input type="number" id="num6"><br>

	<button onclick="obtenerPromedio()">Calcular promedio</button>

	<div id="resultado"></div>
</body>
</html>
