// Dados 4 números y almacénelo en un vector, luego obtenga la suma y el promedio de los
// valores almacenados.
<!DOCTYPE html>
<html>
<head>
<link rel=""href="estilo.css">

	<title>Suma y promedio de números</title>
	<script>
		function calcular() {
			// Obtener los valores ingresados por el usuario
			var num1 = parseFloat(document.getElementById("num1").value);
			var num2 = parseFloat(document.getElementById("num2").value);
			var num3 = parseFloat(document.getElementById("num3").value);
			var num4 = parseFloat(document.getElementById("num4").value);
            var numeros = [num1, num2, num3, num4];

			
			var suma = 0;
			for (var i = 0; i < numeros.length; i++) {
				suma += numeros[i];
			}

			
			var promedio = suma / numeros.length;

			
			document.getElementById("resultado").innerHTML = "La suma es: " + suma + "<br>El promedio es: " + promedio;
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="num1">Número 1:</label>
	<input type="number" id="num1"><br>

	<label for="num2">Número 2:</label>
	<input type="number" id="num2"><br>

	<label for="num3">Número 3:</label>
	<input type="number" id="num3"><br>

	<label for="num4">Número 4:</label>
	<input type="number" id="num4"><br>

	<button onclick="calcular()">Calcular</button>

	<div id="resultado"></div>
</body>
</html>

