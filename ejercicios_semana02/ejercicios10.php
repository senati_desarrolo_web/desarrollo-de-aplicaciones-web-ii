<!DOCTYPE html>
<html>
<head>
	<title>Encriptar frase con valor ASCII</title>
	<script>
		function encriptar() {
			// Obtener la frase del usuario
			var frase = document.getElementById("frase").value;

			// Convertir la frase en un array de caracteres
			var caracteres = frase.split("");

			// Inicializar la frase encriptada
			var fraseEncriptada = "";

			// Iterar sobre cada caracter en el array
			for (var i = 0; i < caracteres.length; i++) {
				// Obtener el valor ASCII del caracter actual
				var valorASCII = caracteres[i].charCodeAt(0);

				// Sumar 2 al valor ASCII para encriptar el caracter
				var valorEncriptado = valorASCII + 2;

				// Convertir el valor encriptado de vuelta a un caracter y añadirlo a la frase encriptada
				fraseEncriptada += String.fromCharCode(valorEncriptado);
			}

			// Mostrar la frase encriptada en la página
			document.getElementById("resultado").innerHTML = "Frase encriptada: " + fraseEncriptada;
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="frase">Ingrese una frase:</label>
	<input type="text" id="frase"><br>

	<button onclick="encriptar()">Encriptar frase</button>

	<div id="resultado"></div>
</body>
</html>
