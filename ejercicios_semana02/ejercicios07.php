<!DOCTYPE html>
<html>
<head>
	<title>mostrar</title>
	<script>
		function mostrarFraseConAsteriscos() {
			// Obtener la frase del usuario
			var frase = document.getElementById("frase").value;

			// Reemplazar los espacios en blanco por asteriscos
			var fraseConAsteriscos = frase.replace(/ /g, "*");

			// Mostrar la frase con asteriscos en la página
			document.getElementById("resultado").innerHTML = fraseConAsteriscos;
		}
	</script>
</head>
<body>
<link rel="stylesheet"href="estilo.css">
	<label for="frase">Ingrese una frase:</label>
	<input type="text" id="frase">

	<button onclick="mostrarFraseConAsteriscos()">Mostrar frase con asteriscos</button>

	<div id="resultado"></div>
</body>
</html>
